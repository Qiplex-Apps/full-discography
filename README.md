<div align="center">

<h1> <a href="https://qiplex.com/software/full-discography/">Full Discography</a> </h1>
  
<h3> Discover ALL releases of the artists you love! </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/full-discography/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/full-discography/">my website</a>
<br>Code comes soon.

![Full Discography](https://qiplex.com/assets/img/app/main/full-discography-app.png)

<h4>Check out the app features below: </h4>

![Full Discography - Features](https://user-images.githubusercontent.com/32670415/147224557-16821a05-8dd5-4c3b-a36d-065099e46a46.png)
  
</div>
